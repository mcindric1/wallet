﻿namespace Serijalizacija {
  partial class FormWallet {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.cbCurrencies = new System.Windows.Forms.ComboBox();
            this.lblBalance = new System.Windows.Forms.Label();
            this.scControls = new System.Windows.Forms.SplitContainer();
            this.btnAddCurrency = new System.Windows.Forms.Button();
            this.btnUpdateCurrency = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.scControls)).BeginInit();
            this.scControls.Panel1.SuspendLayout();
            this.scControls.Panel2.SuspendLayout();
            this.scControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbCurrencies
            // 
            this.cbCurrencies.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cbCurrencies.FormattingEnabled = true;
            this.cbCurrencies.Location = new System.Drawing.Point(12, 12);
            this.cbCurrencies.Name = "cbCurrencies";
            this.cbCurrencies.Size = new System.Drawing.Size(678, 23);
            this.cbCurrencies.TabIndex = 0;
            this.cbCurrencies.SelectedIndexChanged += new System.EventHandler(this.cbCurrencies_SelectedIndexChanged);
            // 
            // lblBalance
            // 
            this.lblBalance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblBalance.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lblBalance.Location = new System.Drawing.Point(12, 61);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(678, 196);
            this.lblBalance.TabIndex = 1;
            this.lblBalance.Text = "0.000000000 BTC";
            this.lblBalance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
    // 
            // scControls
            // 
            this.scControls.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.scControls.Location = new System.Drawing.Point(12, 306);
            this.scControls.Name = "scControls";
            // 
            // scControls.Panel1
            // 
            this.scControls.Panel1.Controls.Add(this.btnAddCurrency);
            // 
            // scControls.Panel2
            // 
            this.scControls.Panel2.Controls.Add(this.btnUpdateCurrency);
            this.scControls.Size = new System.Drawing.Size(678, 56);
            this.scControls.SplitterDistance = 339;
            this.scControls.TabIndex = 2;
            // 
            // btnAddCurrency
            // 
            this.btnAddCurrency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddCurrency.Location = new System.Drawing.Point(0, 0);
            this.btnAddCurrency.Name = "btnAddCurrency";
            this.btnAddCurrency.Size = new System.Drawing.Size(339, 56);
            this.btnAddCurrency.TabIndex = 0;
            this.btnAddCurrency.Text = "Unos valute";
            this.btnAddCurrency.UseVisualStyleBackColor = true;
            this.btnAddCurrency.Click += new System.EventHandler(this.btnAddCurrency_Click);
            // 
            // btnUpdateCurrency
            // 
            this.btnUpdateCurrency.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdateCurrency.Location = new System.Drawing.Point(0, 0);
            this.btnUpdateCurrency.Name = "btnUpdateCurrency";
            this.btnUpdateCurrency.Size = new System.Drawing.Size(335, 56);
            this.btnUpdateCurrency.TabIndex = 0;
            this.btnUpdateCurrency.Text = "Ažuriranje valute";
            this.btnUpdateCurrency.UseVisualStyleBackColor = true;
            this.btnUpdateCurrency.Click += new System.EventHandler(this.btnUpdateCurrency_Click);
            // 
            // FormWallet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 374);
            this.Controls.Add(this.scControls);
            this.Controls.Add(this.lblBalance);
            this.Controls.Add(this.cbCurrencies);
            this.Name = "FormWallet";
            this.Text = "CryptoWallet";
            this.scControls.Panel1.ResumeLayout(false);
            this.scControls.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scControls)).EndInit();
            this.scControls.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private ComboBox cbCurrencies;
    private Label lblBalance;
    private SplitContainer scControls;
    private Button btnAddCurrency;
    private Button btnUpdateCurrency;
  }
}