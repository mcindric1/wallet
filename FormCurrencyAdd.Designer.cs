﻿namespace Serijalizacija {
  partial class FormCurrencyAdd {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
            this.lblCurrencyName = new System.Windows.Forms.Label();
            this.tbCurrencyName = new System.Windows.Forms.TextBox();
            this.tbCurrencyShortName = new System.Windows.Forms.TextBox();
            this.lblCurrencyShortName = new System.Windows.Forms.Label();
            this.tbBalance = new System.Windows.Forms.TextBox();
            this.lblBalance = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCurrencyName
            // 
            this.lblCurrencyName.AutoSize = true;
            this.lblCurrencyName.Location = new System.Drawing.Point(12, 9);
            this.lblCurrencyName.Name = "lblCurrencyName";
            this.lblCurrencyName.Size = new System.Drawing.Size(74, 15);
            this.lblCurrencyName.TabIndex = 0;
            this.lblCurrencyName.Text = "Naziv valute:";
            // 
            // tbCurrencyName
            // 
            this.tbCurrencyName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCurrencyName.Location = new System.Drawing.Point(12, 27);
            this.tbCurrencyName.Name = "tbCurrencyName";
            this.tbCurrencyName.Size = new System.Drawing.Size(381, 23);
            this.tbCurrencyName.TabIndex = 1;
            // 
            // tbCurrencyShortName
            // 
            this.tbCurrencyShortName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbCurrencyShortName.Location = new System.Drawing.Point(12, 71);
            this.tbCurrencyShortName.Name = "tbCurrencyShortName";
            this.tbCurrencyShortName.Size = new System.Drawing.Size(381, 23);
            this.tbCurrencyShortName.TabIndex = 3;
            // 
            // lblCurrencyShortName
            // 
            this.lblCurrencyShortName.AutoSize = true;
            this.lblCurrencyShortName.Location = new System.Drawing.Point(12, 53);
            this.lblCurrencyShortName.Name = "lblCurrencyShortName";
            this.lblCurrencyShortName.Size = new System.Drawing.Size(119, 15);
            this.lblCurrencyShortName.TabIndex = 2;
            this.lblCurrencyShortName.Text = "Skraćeni naziv valute:";
            // 
            // tbBalance
            // 
            this.tbBalance.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbBalance.Location = new System.Drawing.Point(12, 115);
            this.tbBalance.Name = "tbBalance";
            this.tbBalance.Size = new System.Drawing.Size(381, 23);
            this.tbBalance.TabIndex = 5;
            // 
            // lblBalance
            // 
            this.lblBalance.AutoSize = true;
            this.lblBalance.Location = new System.Drawing.Point(12, 97);
            this.lblBalance.Name = "lblBalance";
            this.lblBalance.Size = new System.Drawing.Size(42, 15);
            this.lblBalance.TabIndex = 4;
            this.lblBalance.Text = "Stanje:";
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAdd.Location = new System.Drawing.Point(12, 161);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(381, 36);
            this.btnAdd.TabIndex = 6;
            this.btnAdd.Text = "Unesi";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // FormCurrencyAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 209);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.tbBalance);
            this.Controls.Add(this.lblBalance);
            this.Controls.Add(this.tbCurrencyShortName);
            this.Controls.Add(this.lblCurrencyShortName);
            this.Controls.Add(this.tbCurrencyName);
            this.Controls.Add(this.lblCurrencyName);
            this.Name = "FormCurrencyAdd";
            this.Text = "Unos valute";
            this.ResumeLayout(false);
            this.PerformLayout();

    }

    #endregion

    private Label lblCurrencyName;
    private TextBox tbCurrencyName;
    private TextBox tbCurrencyShortName;
    private Label lblCurrencyShortName;
    private TextBox tbBalance;
    private Label lblBalance;
    private Button btnAdd;
  }
}