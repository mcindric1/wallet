﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Serijalizacija {
    public class CryptoCurrency {
        public CryptoCurrency(string name, string value, string kratica) {
            Name = name;
            Value = value;
            Kratica = kratica;


        }

        public string Name { get; set; }
        public string Value { get; set; }
        public string Kratica { get; set; }

        public string Balance {  get {
                return Value + " " + Kratica;
            } }
        override public string ToString() {
            return Name + " " + Value + " " + Kratica;

        }

    }
}    
