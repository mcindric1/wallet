namespace Serijalizacija {
    public partial class FormWallet : Form {
        public Wallet wallet;

        public FormWallet() {
            InitializeComponent();
            wallet = new Wallet();
            //wallet.Serialize();
            wallet.Deserialize();
            osvjeziLista();
        }

        private void btnAddCurrency_Click(object sender, EventArgs e) {
            FormCurrencyAdd formCurrencyAdd = new FormCurrencyAdd();
            if (formCurrencyAdd.ShowDialog() == DialogResult.OK) {
                CryptoCurrency? newCurrency = formCurrencyAdd.NewCurrency;
                if (newCurrency != null) {
                    wallet.AddCurrency(newCurrency);
                   
                }
            }
        }
        private void osvjeziLista() {
            cbCurrencies.Items.Clear();
            cbCurrencies.Items.AddRange(wallet.GetCurrencies().ToArray());
           
        }

        private void btnUpdateCurrency_Click(object sender, EventArgs e) {
            osvjeziLista();
        }

        private void cbCurrencies_SelectedIndexChanged(object sender, EventArgs e) {
            lblBalance.Text = ((CryptoCurrency)cbCurrencies.SelectedItem).Balance;
            
           
        }

    }
}